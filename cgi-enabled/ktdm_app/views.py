from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from .models import Post, Category
# Create your views here.

def post_list(request):
    posts =  Post.objects.filter(published_at__lte=timezone.now()).order_by('published_at')

    return render(request, 'blog/base_post_list.html', {'posts': posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/base_post_detail.html', {'post': post})

def category_list(request):
    categories = Category.objects.all() # this will get all categories, you can do some filtering if you need (e.g. excluding categories without posts in it)

    return render(request, 'blog/base_category_list.html', {'categories': categories}) # blog/category_list.html should be the template that categories are listed.

def category_detail(request, pk):
    category = get_object_or_404(Category, pk=pk)

    return render(request, 'blog/base_category_post_list.html', {'category': category}) # in this template, you will have access to category and posts under that category by (category.post_set).
