$(document).ready(function() {

	var validator = $('#ajax-form').validate({
		onfocusout: false,
		debug: true,
		rules: {
			'vardas': {
				required: true,
				minlength: 4
			},
			'alga': "required"
		},
		messages: {
			'vardas': {
				required: "Įveskite vardą",
				minlength: jQuery.validator.format("Įveskite bent {0} simbolius")
			},
			'alga': {
				required: "Įveskite algą"
				// minlength: jQuery.validator.format("Įveskite bent {0} simbolius")
			}
		},
		// the errorPlacement has to take the table layout into account
		// errorPlacement: function(error, element) {
		//     if (element.is(":radio"))
		//         error.appendTo(element.parent().next().next());
		//     else if (element.is(":checkbox"))
		//         error.appendTo(element.next());
		//     else
		//         error.appendTo(element.parent().next());
		// },
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		},
		submitHandler: function() {
			// your ajax or form submit here
			alert('form is valid');
			return false;
		}

	});


	table = $('#lentele').DataTable({
		"ajax": 'next-ajax-datatable.php'
	});

	$('input[name=alga]').numeric(
		false,
		function() {
			alert('Integers only');
			this.value = '';
			this.focus();
		}
	);
	$('input[name=subm]').submit(function() {
		if (
			validator.valid())
		// Boolean($('input[name=vardas]').val()) && isNaN($('input[name=vardas]').val()) && Boolean($('input[name=alga]').val()) && $('input[name=alga]').numeric())
		{
			var data = { 'vardas': $('input[name=vardas]').val(), 'alga': $('input[name=alga]').val() };

			$.ajax({
				method: "POST",
				url: "next-ajax-datatable.php",
				dataType: 'json',
				data: data,
				success: function(data) {
					table.ajax.reload();
					$('input[name=vardas]').val('');
					$('input[name=alga]').val('');
				}
			});
		}
		// else {
		//     alert('Prašome užpildyti formą');
		// }
	});

	$('input[name=refre]').click(function() {
		table.ajax.reload();
	});
});