$(document).ready(function() {
    $('#clear').click(function(event) {
        $('#txtHint').html("<b>Loading response...</b>");
        $.post('uzduotis.php', { func: 1 }, function(data) {
            $('#txtHint').html(data);
            console.log(data);
        });
        return false;

    });
    $('#load').click(function(event) {
        $.post('uzduotis.php', { func: 0 }, function(data) {
            $('#txtHint').html(data);
            console.log(data);
        });
        return false;
    });
});