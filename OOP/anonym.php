<?php
error_reporting(E_ALL);
function increment(&$var)
{
    $var++;
}

$a = 0;
call_user_func('increment', $a);
echo $a."\n";

// You can use this instead
call_user_func_array('increment', array(&$a));
echo $a."\n";



class a {
    const aaa = 'xxx';
    function __construct(){
        echo __CLASS__ . ' ' .  self::aaa . ' construct<br>';
    }
    function __destruct(){
        echo __CLASS__ .  ' ' . a::aaa . ' destruct<br>';
    }
}
echo '-- pries new--<br>';
$p = new a(); // egzempliorius sukuriamas: suveikia construct
echo '-- po new --<br>';
unset($p); // egzempliorius sunaikinamas: suveikia destruct
echo '-- po unset --<br>';
?>