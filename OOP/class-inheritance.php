<?php

class a {
    private $x;
    public $y;
    function __construct($x, $y){
        $this->x = $x;
        $this->y = $y;

    }
}
class b extends a {
    public $z = 9;

}

$p = new a(1,2);
var_dump($p);
$p = new b(1,2);
var_dump($p);