<?php
/*
Sukurkite PHP skriptą, kuriame būtų aprašyta klasė “automobiliai”, 
kuri turi savybes 
gamintojas, 
modelis, 
metai. 

Sukurkite standartinį klasės __construct metodą, 
kuriam perdavus tris parametrus ‐ gamintojas, modelis, metai ‐ 
perduotus parametrus padėtų į savo savybes. 
Pademonstruoti veikimą.

Panaudodami prieš tai sukurtą klasę automobilis (8.1), 
sukurkite klases lengvasis ir krovininis, 
kurios paveldi automobilis klasę. 
automobilis klasę papildykite metodu “duomenys”, 
kuris išvestų suformatuotą eilutę “$gamintojas - $modelis - $metai”. 
lengvasis klasėje perrašykite metodą duomenys, 
pakeisdami išvedamą eilutę į “gamintojas, modelis, metai (lengvasis automobilis)”. 
lengvasis automobilis ‐ tiesiog paprastas žodis.

Sukurti klasę turgus. 
Sukurti funkciją atomobilis, su kuria būtų galima pateikti automobilį į turgų. 
Funkcijos parametrai: gamintojas, modelis, metai, kaina. 
Automobilių informacijų talpinti į klasės savybę automobiliai (masyvas). 
Sukurti funkciją minKaina, kuri surastų automobilį su mažiausia kaina.
*/
// session_start();
error_reporting(E_WARNING);
class automobilis{
    private $gamintojas, $modelis, $metai;
    private $mas = [];
    public $format = "%s - %s - %d";

    function add($a, $b, $c){
        $this->gamintojas = $a;
        $this->modelis = $b;
        $this->metai = $c;
        $this->mas[] = ['gamintojas'=> $this->gamintojas, 'modelis'=> $this->modelis, 'metai'=> $this->metai ];
    }

    function print_format($a){
        
         return vsprintf($this->format . "<br>", $a);
        // var_dump($this->format);
        // echo($a['gamintojas']);
        // var_dump($a->$b['modelis']);
    }

    
    function duomenys($a = true){
        if ($a === true){
            $a = $this->mas;   
        }
        for ($z = 0; $z<count($a); $z++){
            // echo (sprintf($this->format, $this->mas[$z]['gamintojas'],$this->mas[$z]['modelis'], $this->mas[$z]['metai']));
            echo ($this->print_format($a[$z]));
            // var_dump(count($this->mas));
        }
    }

};

$p = new automobilis();
$p->add('BMW', 'M5', 2012);       
$p->add('audi', 'M5', 2012);       
$p->add('ford', 'M5', 2012);  

$p->duomenys();
// $p->print_format($this->mas, $z);
class lengvasis extends automobilis {
public function __construct(){ 
    $this->format .=" (lengvasis automobilis)";
}

}
$p = new lengvasis();
// $p->duomenys();  
class krovininis extends automobilis {
    public function __construct(){ 
        $this->format .= " (krovininis automobilis)";
    }
}

class turgus extends automobilis{

    public function __construct(){ 
        $this->format .= " - kaina: %d";

    }
    function addToMarket($a, $b, $c, $d){
        $this->gamintojas = $a;
        $this->modelis = $b;
        $this->metai = $c;
        $this->kaina = $d;
        $this->mas[] = ['gamintojas'=> $this->gamintojas, 'modelis'=> $this->modelis, 'metai'=> $this->metai, 'kaina'=> $this->kaina ];
 
    }
    function minKaina(){
        // array_search($this->mas, min($this->mas['kaina']));
        $this->min_array = $this->mas[array_search(min($prices = array_column($this->mas, 'kaina')), $prices)]; 
        // $max_array = $this->mas[array_search(max($prices), $prices)];
        // echo ('masina su maziausia kaina: '. sprintf($this->format, $this->min_array['gamintojas'],$this->min_array['modelis'], $this->min_array['metai'], $this->min_array['kaina']));
        echo 'masina su maziausia kaina: ', $this->print_format($this->min_array);
    }
    function tduomenys(){
        $this->duomenys($this->mas);
        // for ($z = 0; $z<count($this->mas); $z++){
        //     // echo (sprintf($this->format, $this->mas[$z]['gamintojas'],$this->mas[$z]['modelis'], $this->mas[$z]['metai']));
        //     echo ($this->print_format($this->mas[$z]));
        //     var_dump(count($this->mas));
        // }
    }
}

$r = new turgus();
$r->addToMarket('Audi', 'bulka', 1991, 900);
$r->addToMarket('Audi', '100', 1992, 345);
$r->addToMarket('Audi', '46', 1991, 365);
$r->addToMarket('Audi', '344', 1991, 890);
$r->tduomenys();
$r->minKaina();
// var_dump();
?>

