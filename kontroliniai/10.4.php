<?php
/*
Sukurkite PHP skriptą, kuriame aprašykime klasę textList, kurioje būtų viena savybė ‐ $texts, kuri bus masyvas, taip pat būtų metodas add($text), kuris prideda naują tekstą į masyvą sąvybę $texts. Taip pat sukurkite metodą symbols(), kuris grąžintų masyve sąvybėje esančių elementų simbolių skaičių
*/

class textList {
    public $texts = [];
    public function add($data){
        $this->texts[] = $data;
    }
    public function symbols()
    {
       return strlen(implode('', $this->texts));

    }    
}

$p = new textList();
$p->add('lolorolo');
$p->add('lolorolo');
$p->add('lolorolo');
$p->add('lolorolo');
$p->add('lolorolo');
$p->add('lolorolo');
$p->add('lolorolo');
echo json_encode($p->texts);
echo $p->symbols();