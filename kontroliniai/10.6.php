<?php
/*
Sukurkite PHP skriptą, kuriame aprašykite klasę loto, kurioje būtų viena savybė ‐ $table, kuri bus dvimatis masyvas, taip pat būtų metodas generate(), kuris sugeneruoja 5 x 5 atsitiktinių skaičių dvimatį masyvą (min skaičius = 1, max skaičius = 75). Sukurkite metodą get(),kuris išveda sugeneruotą masyvą 5 x 5 HTML lentele. Pademonstruokite veikimą.
*/

class loto  
{
    public $table = [];

    public function generate()
    {
        $sk = rand(1, 75);
        for ($x = 0; $x<5; $x++){
            $this->table[$x] = [rand(1, 75), rand(1, 75), rand(1, 75), rand(1, 75), rand(1, 75)];
        };
        return $this->table;
    }
    public function get()
    {
        echo json_encode($this->generate());
    }
}

switch ($_POST["func"]) {
    case 0:
        $p = new loto();
        $p->get();
}