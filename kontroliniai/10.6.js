$(document).ready(function() {
    $.post('10.6.php', { func: 0 }, function(data) {
        $.each($.parseJSON(data), function(i, item) {
            $('<tr>').append(
                $('<td>').text(item[0]),
                $('<td>').text(item[1]),
                $('<td>').text(item[2]),
                $('<td>').text(item[3]),
                $('<td>').text(item[4])
            ).appendTo('#loto-table');
        });
    });

    $('#load').click(function(event) {
        $('tr').remove();
        $.post('10.6.php', { func: 0 }, function(data) {
            $.each($.parseJSON(data), function(i, item) {
                $('<tr>').append(
                    $('<td>').text(item[0]),
                    $('<td>').text(item[1]),
                    $('<td>').text(item[2]),
                    $('<td>').text(item[3]),
                    $('<td>').text(item[4])
                ).appendTo('#loto-table');
            });
        });
        return false;
    });
});