<?php
/*
Sukurkite PHP skriptą, kuriame aprašykime klasę rndList, kurioje būtų viena savybė ‐ $numbers, kuri bus masyvas, taip pat būtų metodas generate(), kuris sugeneruoja atsitiktini skaičių ir padeda į masyvą sąvybę $numbers. Taip pat sukurkite metodą list(), kuris surikiuoja masyve esančius skaičius ir išveda juos su echo atskiriant vieną nuo kito tarpais.
*/

class rndList {
    public $numbers = [];
    public function generate()
    {
        $this->numbers[] = rand(5,100);
    }
    public function list()  
    {
       sort($this->numbers);
        return implode(" ", $this->numbers);
    }
}
$p = new rndList();
$p->generate();
$p->generate();
$p->generate();
$p->generate();
$p->generate();
$p->generate();
$p->generate();
var_dump($p->numbers);
echo $p->list();