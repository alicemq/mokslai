#!/usr/bin python3
# -*- coding: utf-8 -*-
# Autorius Žygimantas Markevičius


def splt():
    while True:
        try:

            # prašome vartotoją įvesti duomenis
            vart_inp = input('Įveskite asmens kodą: ')

            # tikriname duomenų ilgį ir ar visi simboliai yra skaiciai
            if len(vart_inp) != 11 or vart_inp.isdigit() == False:
                print('Asmens kodas per trumpas arba yra neleistinu simbboliu')
                return splt()

            inp = list(str(vart_inp))  # vartotojo duomenis sukeliame į sąrašą
            # simboliai, kuriuos naudosime kaip žodyno raktus ("key")
            chars = list('abcdefghijk')
            # sukuriame žodyną su raktais ir numatyta reikšme "0"
            asm_k = dict.fromkeys(chars, '')
            # ciklas nurodant sritį kurioje dirbsime - nuo index 0 iki dinamiškai kintančio sąrašo ilgio
            for ii in range(0, len(inp)):
                # priskiriame reikšmes iš inp sąrašo į asm_k žodyną
                asm_k[chars[ii]] = int(inp[ii])

            # sukuriame trumpus kintamuosiuos iš žodyno reikšmių, pvz: a = 3, b = 5 ir t.t.
            for key, value in asm_k.items():
                globals()[key] = value
            # sukuriame žodyną su numatyta reikšme "nežinoma"
            lytis = dict.fromkeys(range(1, 10), 'nežinoma')
            # į žodyną įterpiame norimas reikšmes, ciklas vykdomas srityje nuo 1 iki 6, žingsnio ilgis - 2
            for ii in range(1, 6, 2):
                lytis[ii] = 'vyras'  # itraukiama reikšmė
                # įtraukiama reikšmė sekančiam laukeliui (+1)
                lytis[ii+1] = 'moteris'

            # darome kintamuosius su metais, mėnesiu ir diena naudojant žodyno reikšmes
            kmetai = int(str(asm_k['b']) + str(asm_k['c']))
            kmenuo = int(str(asm_k['d']) + str(asm_k['e']))
            kdiena = int(str(asm_k['f']) + str(asm_k['g']))
            # darome pilną metų skaičių naudojantis žodynais
            metai_pre = {1: '18', 2: '18', 3: '19',
                         4: '19', 5: '20', 6: '20', 9: ''}
            metai = int(str(metai_pre[int(asm_k['a'])]) + str(kmetai))

            # tikriname ar metai yra keliamieji ir priskiriame vasariui tinkamą dienų skaičių
            if (metai % 4) == 0:
                if (metai % 100) == 0:
                    if (metai % 400) == 0:
                        vasaris_d = 29
                    else:
                        vasaris_d = 28
                else:
                    vasaris_d = 29
            else:
                vasaris_d = 28

            # tikriname ar reikšmės nėra didesnės nei leidžiamos
            if asm_k['a'] != 9:
                if kmenuo > 12 or kdiena > 31 or asm_k['a'] in [7, 8] or kmenuo == 2 and kdiena > vasaris_d:
                    print('Asmens kodas neteisingas')
                    return splt()

            # skaičiuojame pagal algoritmą https://lt.wikipedia.org/wiki/Asmens_kodas
            asm_kon = ((a*1)+(b*2)+(c*3)+(d*4)+(e*5) +
                       (f*6)+(g*7)+(h*8)+(i*9)+(j*1))
            asm_kon2 = a*3 + b*4 + c*5 + d*6 + e*7 + f*8 + g*9 + h*1 + i*2 + j*3
            asm_kont_check = asm_kon % 11
            asm_kont_check2 = asm_kon2 % 11
            asm_kod_kont = ''
            if asm_k['a'] != 9:
                if asm_kont_check == 10:
                    if asm_kont_check2 == 10:
                        asm_kod_kont = 0
                    else:
                        asm_kod_kont = asm_kont_check2
                else:
                    asm_kod_kont = asm_kont_check

            # jei kontrolinis numeris sutampa su nurodytu, spausdiname paaiškinamąją informaciją
            if asm_kod_kont == asm_k['k'] or asm_k['a'] == 9:
                if asm_k['a'] in {7, 8, 9}:
                    metai_print = lytis_print = eil_nr = asm_k['k'] = 'nenustatyta'
                else:
                    metai_print = '%s-%s-%s' % (metai, kmenuo, kdiena)
                    lytis_print = lytis[asm_k['a']]
                    eil_nr = '%s%s%s' % (asm_k['h'], asm_k['i'], asm_k['j'])
                print ('Lytis: %s' % lytis_print)
                print ('Gimimo data: %s' % metai_print)
                print ('Eilės numeris: %s' % eil_nr)
                print ('Kontrolinis numeris: %s' % asm_k['k'])
            else:
                print('asmens kodas neteisingas')

        except ValueError:
            print("Neteisingas formatas")
        except AssertionError:
            print("Please enter an integer between 1 and 11")
        else:
            return splt()


splt()
