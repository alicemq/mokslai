<?php 
$m = [1,2,3,4,5,6];


$m = [
    ['vardas' => 'Jonas', 'amzius' => '34'],
    ['vardas' => 'Petras', 'amzius' => '56'],
    ['vardas' => 'Tomas', 'amzius' => '35'],
    ['vardas' => 'Jonis', 'amzius' => '54'],
    ['vardas' => 'Simonas', 'amzius' => '13'],
    ['vardas' => 'Antanas', 'amzius' => '67']
];

usort($m, function($a, $b) {
    if ($a['amzius'] == $b['amzius']) return 0;
    elseif ($a['amzius'] > $b['amzius']) return 1;
    else return -1;

});

echo json_encode($m) . '<br>';
usort($m, function($a, $b) {
   return strcmp($a['vardas'], $b['vardas']);

});

echo json_encode($m) . '<br>';

$a = 1;
$b = 2;

echo $a > $b ?: 'lol';

$vardai = ['JOnAs', 'pETras', 'AnTanAS', 'SiMoNAS', 'ToMaS', 'lUKaS', 'kAROLiNa'];

function corrcase($data) {
    foreach($data as $ftu){
       $varrdai[] =  ucfirst(strtolower($ftu));
    }
    unset($ftu);
    return $varrdai;
};

echo json_encode(corrcase($vardai));

$skai = [1,2,3,4,5,6,7,8,9,0,6,5,4,3,2,6,7,5,3];

function tonull($data){
    $masyv = [];
    foreach ($data as $param){
         $param % 2 == 0 ? ($masyv[] = $param) : ($masyv[] = 0);
    }
    return $masyv;
};
echo json_encode(tonull($skai));