<div class="container  mx-auto">
    <!-- inner container -->
    <div class="row">
        <!-- offset by 4  -->
        <div class="col-sm-3 col-md-3">
        </div>
        <!-- end of offset by 4 -->

        <!-- calc -->
        <div class="col-sm-6 col-md-6">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 p-2 input-group input-group-lg">
                    <input type="text" class="form-control text-right" aria-label="Screen">
                </div>
            </div>
            <div class="row">
                <div class=" col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="clr" class="btn btn-primary btn-lg btn-block" type="button">C</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="clr_curr" value="" class="btn btn-primary btn-lg btn-block" type="button">CE</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="%" value="%" class="btn btn-primary btn-lg btn-block" type="button">%</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="division" value="" class="btn btn-primary btn-lg btn-block" type="button">&#247;</button>
                </div>
            </div>

            <div class="row">
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="7" value="" class="btn btn-primary btn-lg btn-block" type="button">7</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="8" value="" class="btn btn-primary btn-lg btn-block" type="button">8</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="9" value="" class="btn btn-primary btn-lg btn-block" type="button">9</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="multiply" value="" class="btn btn-primary btn-lg btn-block" type="button">X</button>
                </div>
            </div>

            <div class="row">
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="4" value="" class="btn btn-primary btn-lg btn-block" type="button">4</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="5" value="" class="btn btn-primary btn-lg btn-block" type="button">5</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="6" value="" class="btn btn-primary btn-lg btn-block" type="button">6</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="minus" value="" class="btn btn-primary btn-lg btn-block" type="button">&ndash;</button>
                </div>
            </div>

            <div class="row">
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="1" value="" class="btn btn-primary btn-lg btn-block" type="button">1</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="2" value="" class="btn btn-primary btn-lg btn-block" type="button">2</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="3" value="" class="btn btn-primary btn-lg btn-block" type="button">3</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="plus" value="" class="btn btn-primary btn-lg btn-block" type="button">+</button>
                </div>
            </div>

            <div class="row">
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="0" value="" class="btn btn-primary btn-lg btn-block" type="button">0</button>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 p-2">
                    <button form="calc" name="comma" value="" class="btn btn-primary btn-lg btn-block" type="button">,</button>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 p-2">
                    <button form="calc" name="equal" value="" class="btn btn-primary btn-lg btn-block" type="button">=</button>
                </div>
            </div>

        </div>
        <!-- end of calc -->

        <!-- offset by 4  -->
        <div class="col-sm-3 col-md-3">
        </div>
        <!-- end of offset by 4 -->
    </div>
    <!-- end of inner container -->
</div>