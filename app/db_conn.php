<?php
include "config.php";
try {
    $pdo = new PDO($host, $user, $pass, array(
    PDO::ATTR_PERSISTENT => true, 
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ));
    // echo $conn_est;
    // $pdo = null;
} catch (PDOException $e) {
    print "Klaida!: " . $e->getMessage() . "<br/>";
    die();
}
?>