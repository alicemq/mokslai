<div>
<form class="form-horizontal" action="backend.php" method="POST">
<fieldset>

<!-- Form Name -->

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="var"></label>  
  <div class="col-md-4">
  <input id="var" name="var" type="text" placeholder="Vardas" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pav"></label>  
  <div class="col-md-4">
  <input id="pav" name="pav" type="text" placeholder="Pavardė" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="amz"></label>  
  <div class="col-md-4">
  <input id="amz" name="amz" type="text" placeholder="Amzius" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="alg"></label>  
  <div class="col-md-2">
  <input id="alg" name="alg" type="text" placeholder="Alga" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Pateikti</button>
  </div>
</div>

</fieldset>
</form>



</div>